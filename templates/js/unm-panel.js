
$(".unm_panel_open").click(function(a) {
        a.preventDefault(), $(".unm_panel_open .caret").toggleClass("up");
        $("#unm_panel").slideToggle();
});

$(function(a) {
    a.getJSON("//webcore.unm.edu/json.php?content=v2/unm-panel.html").done(function (b) {
        a("#unm_panel .container").append(b.content)
    }).fail(function (a, b, c, d) {
        console.log("Error data:", b), console.log(d + " " + c), console.log("readyState: " + a.readyState + "\nstatus: " + a.status + "\nresponseText: " + a.responseText)
    })
});

$(function(a) {
    a("#totop").click(function () {
        return a("html, body").animate({scrollTop: 0}), !1
    }), a(window).scroll(function () {
        a(window).scrollTop() > 200 ? a("#totop").fadeIn() : a("#totop").fadeOut()
    })
});

