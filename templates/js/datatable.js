/**
 * Initialize the DataTable
 */
$('.data-table').DataTable({
    dom:
    "<'row'<'col-xs-12 col-sm-6'l><'col-xs-12 col-sm-6'f>>" +
    "<'row'<'col-sm-12'tr>>" +
    "<'row'<'col-sm-4'i><'col-sm-8'p>>",
    renderer: 'bootstrap'
});
