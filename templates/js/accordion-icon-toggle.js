/**
 * Toggles the icon from right to down and vice-versa
 * Grandparent must have the .accordion class and the child must contain an <i class="indicator"> tag
 * @Example HTML Markup
<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title accordion" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                <a class="no_underline h4">1. What is HTML?
                    <i class="fa fa-chevron-down indicator pull-right"></i></a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in">
            <div class="panel-body">
                <p>HTML stands for HyperText Markup Language. HTML is the main markup language for describing the structure of Web pages. <a href="http://www.tutorialrepublic.com/html-tutorial/" target="_blank">Learn more.</a></p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title accordion" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                <a class="no_underline h4">2. What is Bootstrap?
                    <i class="fa fa-chevron-right indicator pull-right"></i></a>
            </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse">
            <div class="panel-body">
                <p>Bootstrap is a powerful front-end framework for faster and easier web development. It is a collection of CSS and HTML conventions. <a href="http://www.tutorialrepublic.com/twitter-bootstrap-tutorial/" target="_blank">Learn more.</a></p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title accordion" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                <a class="no_underline h4">3. What is CSS?
                    <i class="fa fa-chevron-right indicator pull-right"></i></a>
            </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse">
            <div class="panel-body">
                <p>CSS stands for Cascading Style Sheet. CSS allows you to specify various style properties for a given HTML element such as colors, backgrounds, fonts etc. <a href="http://www.tutorialrepublic.com/css-tutorial/" target="_blank">Learn more.</a></p>
            </div>
        </div>
    </div>
</div>
 */

    $(".accordion").click(function(){
      //Reset any collapsed accordion panel with the right facing chevron icon
      $(this).parents(".panel-group").find(".collapsed").each(function() {
          $(this).find("i.indicator").toggleClass("fa-chevron-down fa-chevron-right");
      });

       //Toggle the open accordion panel to the down facing chevron icon
       $(this).find("i.indicator").toggleClass("fa-chevron-down fa-chevron-right");
    });

