/**
 * Created by gaberiv on 3/24/2015.
 * Moves the horizontal top navigation into the navbar for small screens
 */
$("#navbar").on('show.bs.collapse', function(){
    $($("#navbar")).prepend($("#top-nav"));
    $("#navbar").removeClass('pull-right');
    $("#top-nav").removeClass('pull-left');
});
$("#navbar").on('hidden.bs.collapse', function(){
    $($("#horz-nav")).prepend($("#top-nav"));
    $("#navbar").addClass('pull-right');
    $("#top-nav").addClass('pull-left');
});

