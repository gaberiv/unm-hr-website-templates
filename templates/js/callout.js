$(".callout").each(function(){
    var color = $(this).css("background-color");
    var new_color = tinycolor(color).darken(8).toString();
    $(this).css("border-left", "5px solid " + new_color);
});
