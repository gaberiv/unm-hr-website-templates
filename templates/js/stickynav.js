/**
 * Add the sticknav class on scroll or resize.  This will keep the horizontal nav
 * in the users view at all times.
 */
$(function () {
    $(window).one("scroll", function () {
        sticky_navigation_offset_top = $("#nav").offset().top
    }),
        $(window).resize(function () {
            sticky_navigation_offset_top = $("#nav").removeClass("stickynav").offset().top + $("nav.navbar").height(), a()
        }),
        $(window).scroll(function () {
            a()
        });
    var a = function () {
        var a = $(window).scrollTop();
        a > sticky_navigation_offset_top ? $("#nav").addClass("stickynav") : $("#nav").removeClass("stickynav")
    }
});
